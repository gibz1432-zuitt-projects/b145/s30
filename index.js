

// Get TITLES

fetch('https://jsonplaceholder.typicode.com/todos')
	.then( response => response.json() )
	// .then ( result => console.log(result))
	.then( json => json.map((i) =>{
		return (i.title)
	}))
	.then ( todo => console.log(todo));



// GET 

fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then( response => response.json() )
	.then ( todo => {
		console.log(todo)
		console.log(`The item "${todo.title}" on the list has a status of ${todo.completed}`)
	});



//POST

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type' : 'application/json'
	},
		body: JSON.stringify({
		completed: false,
		id: 201,
		title: 'Created To Do List Item',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


//PUT

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,
		status: "Pending",
		title: 'Updated To do List Item',
		userId: 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));


//PATCH
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		id: 1,
		status: "Complete",
		title: 'delectus aut autem',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


//DELETE

fetch('https://jsonplaceholder.typicode.com/todos/500', {
	method: 'DELETE'
})

